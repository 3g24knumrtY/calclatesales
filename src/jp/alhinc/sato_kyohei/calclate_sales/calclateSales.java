package jp.alhinc.sato_kyohei.calclate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalclateSales {
	public static void main(String[] args) {
		if (args.length != 1) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}
		Map<String, String> branchNames = new HashMap<>();
		Map<String, Long> branchSales = new HashMap<>();

		//入力結果を表すinputFileメソッドへ移動。if(false)なら終了する処理。
		if(!inputFile(args[0],  "branch.lst", branchNames, branchSales,"支店",  "^[0-9]{3}$")) {
			return;
		}

		//全てのファイルからを売上ファイルだけを取得してrcdfilesに格納する処理。
		File[] files = new File(args[0]).listFiles(); //argsにあるファイルの情報がfilesへ格納
		List<File> rcdfiles = new ArrayList<>();
		for (int i = 0; i < files.length; i++) {
			files[i].getName(); //ファイル名を取得
			String fileName = files[i].getName(); //変数fileNamesに全ファイルを取得
			if (files[i].isFile() && fileName.matches("^[0-9]{8}.rcd$")) { //ファイルなのか確認する処理
				rcdfiles.add(files[i]); //rcdfilesは数字8桁.rcdのみの売上ファイル。
			}
		} //売上ファイルが連番か確認する方法。
		for (int i = 0; i < rcdfiles.size() - 1; i++) {
			File file = rcdfiles.get(i); //リスト型から0番目のデータを取得してfileに代入。
			String fileName = file.getName(); //ファイルの名前だけを取得して文字列としてfileNameに格納
			File file2 = rcdfiles.get(i + 1); //リスト型からi＋１番目のデータを取得してfile2に代入。
			String fileName2 = file2.getName(); //ファイル2の名前だけを取得して文字列のfileName2に代入。
			int former = Integer.parseInt(fileName.substring(0, 8));
			int latter = Integer.parseInt(fileName2.substring(0, 8));
			if ((latter - former) != 1) {
				System.out.println("売上ファイル名が連番になっていません。");
				return;
			}
		}
		//売上ファイルを読み込み、支店コードと売上情報を抽出し、その該当する支店へ加算し、再度マップへ代入する処理。
		for (int i = 0; i < rcdfiles.size(); i++) {
			BufferedReader brSales = null;
			try {
				File file = rcdfiles.get(i);
				FileReader fr = new FileReader(file);
				brSales = new BufferedReader(fr);

				String line;
				List<String> sales = new ArrayList<>(); //売上ファイルの中身salers(支店コード、売上金額)
                String saleFileName= file.getName();
				while ((line = brSales.readLine()) != null) {
					sales.add(line); //salersの中身の売上ファイルの内容
				}

				if (sales.size() != 2) { //売上フォーマットが2行かを確認する処理
					System.out.println("<" + saleFileName+ ">のフォーマットが不正です");
					return;
				}
				//Mapに特定のキーがあるかを確認する処理
				if (!branchNames.containsKey(sales.get(0))) {
					System.out.println("<" + saleFileName + ">の支店コードが不正です");
					return;
				}

				if (!sales.get(1).matches("^[0-9]*$")) { //売上金額が数字なのかを確認する処理
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
				long fileSale = Long.parseLong(sales.get(1)); //マップに追加するための型変換の処理。faleSaleを作成し、salesに入っている売上情報を代入した。
				long mapSale = branchSales.get(sales.get(0)); //マップにある売上金額の情報を取得したいからgetを使う。mapSaleを作成し、keyを指定してvalueを引き出し代入。
				long saleAmount = mapSale + fileSale; //既にマップに保存してある値と新しく作った売上ファイルにある合計金額を足す。
				if (saleAmount >= 10000000000L) { //
					System.out.println("合計金額が10桁を超えました");
					return;
				}
				branchSales.put(sales.get(0), saleAmount);

			} catch (IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return;
			} finally {
				if (brSales != null) {
					try {
						brSales.close();
					} catch (IOException e) {
						System.out.println("予期せぬエラーが発生しました");
						return;
					}
				}
			}
		}

		//集計結果を表すoutputFileメソッドへ移動。if(false)なら終了する処理。
		if (!outputFile(args[0], "branch.out", branchNames, branchSales)) {
			return;
		}

	}
	//あるファイルから情報を入力する処理
    private static boolean inputFile(String path, String fileName, Map<String, String> mapNames, Map<String, Long> mapSales, String definition, String judgement) {
    	BufferedReader br = null;
    	try {
    		File file = new File(path, fileName);
    		if (!file.exists()) {
    			System.out.println(definition + "定義ファイルが存在しません。");
    			return false;
    		}
    		FileReader fr = new FileReader(file);
    		br = new BufferedReader(fr);

    		String line;
    		while ((line = br.readLine()) != null) {
    			String[] items = line.split(","); //支店コードと支店名の保持が完了
    			if ((items.length != 2) || (!items[0].matches(judgement))) {
    				System.out.println(definition +"定義ファイルのフォーマットが不正です。");
    				return false;
    			}
    			mapNames.put(items[0], items[1]); //Map.Namesに支店コード、支店名を保持
    			mapSales.put(items[0], 0L); //Map.Salesに支店コード、売上情報を保持

    		}

    	} catch (IOException e) {
    		System.out.println("予期せぬエラーが発生しました");
    		return false;
    	} finally {
    		if (br != null) {
    			try {
    				br.close();
    			} catch (IOException e) {
    				System.out.println("予期せぬエラーが発生しました");
    				return false;
    			}
    		}
    	}
    	return true;
    }


	//集計結果をあるファイルに出力する処理。
    private static boolean outputFile(String path, String fileName, Map<String, String> mapNames,
			Map<String, Long> mapSales) {
		BufferedWriter bw = null;
		try {
			File file = new File(path, fileName);
			FileWriter fw = new FileWriter(file);
			bw = new BufferedWriter(fw);
			for (String key : mapNames.keySet()) { //支店コード取得完了 keyに代入
				bw.write(key + "," + mapNames.get(key) + "," + mapSales.get(key)); //支店コードと支店名と売上金額をファイルに出力。カンマをつけるときのことを忘れないで！
				bw.newLine(); //ここで改行
			}
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			if (bw != null) {
				try {
					bw.close();
				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}
}
